import './App.css'

function App() {
  const data: {name: string, age: number, granduationCourse: string, isEnrolled:boolean, favoriteSubject?: string} = {
    name: "Diego",
    age: 20,
        granduationCourse: "Engenharia Química",
        isEnrolled: true,
        favoriteSubject: "Fluidmechanics"
  }

  return (
    <>
    <h1>Sistema de cadastros e gestão</h1>
    <h2>Dados de pesquisador</h2>
      <img className='logo' src="src/assets/lccvlogo.png" alt="" />
      <div className='basic-informations'>
      <p className='content-container name'>Nome: {data.name} </p>
      <p className='content-container'>Idade: {data.age}</p>
      </div>
      <div className='graduation-content'>
      <p className='content-container large'>Graduando em: {data.granduationCourse}</p>
      <p className='content-container large'>Está matriculado? {data.isEnrolled && "Sim"}</p>
      <p className='content-container large'>Disciplina favorita (opcional): {data.favoriteSubject}</p>
      </div>
    </>
  )
}

export default App
